<?php require_once("dashbord_nav.php");
    


?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Change Password</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    
        <style>
            .card{
            width:40%;
            top:150px;
            margin:auto;
        }
        h1{
            font-family:serif ;
           
        }
        p{
            
            text-align:center;
            color:grey;
        }
        .btn-primary{
            width:60%;
            margin:auto;
        }
        </style>
    </head>
<form action="" method="post">
    <div class="container">
        <div class="card">
            <div class="card-header">Change Password</div>
                <div class="card-body">
                    <div class="form-group">
                        <input type="text" name="old_pass" class="form-control" placeholder="Old password">
                    </div>
                    <div class="form-group">
                            <input type="text" name="new_pass" class="form-control" placeholder="New password">
                    </div>
                    <div class="form-group">
                            <input type="text" name="con_new_pass" class="form-control" placeholder="Re-Enter password">
                    </div>
                    <div class="form-group">
                            <input type="submit" name="submit" class="form-control btn-primary" value="Change Password">
                    </div>
                </div>    
           
        </div>
    </div>
</form>
