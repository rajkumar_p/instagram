<?php require_once("dashbord_nav.php");
 
    session_start();
    require_once("show_user_edit_data.php");
    require_once("show_profile_images.php");

    if(isset($_SESSION['naam'])){
    
     
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profile</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>


        
    <style>
        .raj{
            margin-top:100px;
            width:100%;
            
            
                
        }
        .profile{
            width:200px;
            height:200px;
            border-radius:50px;
            margin-left:50px;
        }
        img{
            width:260px;
            height:260px;
            margin-left:40px;
        }
        
        .profile_image{
           
           margin:auto;
        }
        .row{
           
         
          border:1px solid lack;
           
        }
        ul li{
           padding:10px;
           font-size:18px;
        }
        a{
            color:black;
            
        }
        a:hover{
            text-decoration:none;
            color:black;
           
        }
        b{
            font-size:20px;
        }
        /* .col-sm-8{
            border:1px solid lack;
            align:center;
            
        } */
        hr{
            width:84%;
        }
        .image_div{
            width:90%;
        }
        /* .container{
            border:1px solid black;
        } */
        
        
    </style>
</head>
<body>
    <div class="container raj">
         <div class="row">
            <div class="profile_image col-sm-4">
           
            <?php foreach($result as $key => $value) { 
             if(isset($value['image'])){
            ?> 
                <img class="profile" src="profile_image/<?php echo $value['image'];?>" alt="">
             <?php } else{?>
                <img src="default.png" alt="">
             <?php }?>
            </div>   
            <div class=" col-sm-12 col-md-5 col-lg-7" >
            
                <h2><?php echo $value['username'];?></h2><br>
                <nav>
                    <div class="">
                        
                        <ul class="nav">
                            <li><a href="add_friends.php">Add Friend</a></li>
                            <li><a href="#">Friend</a></li>
                            <li><a href="edit_profile.php?id = <?php echo $_SESSION["id"];?>"> Edit Profile</a></li>
                        </ul>
                    </div><br>
                    <div>
                        <b><?php echo $value['full_name'];?></b>
                    </div><br>
                    <div>
                        <p><?php echo $value['Bio'];?></p>
                    </div>
                    
                </nav>
            <?php } ?>
            </div>
        </div>
    </div>
            
    <hr>

    <div class="container">
        <div class="container image_div">
            <div class="row">
            <?php foreach ($image as $key => $value) {
                  ?>
                <div class="  col-sm-12 col-lg-4 col-md-6">
                
                  <img  src="profile_image/<?php echo $value['image'];?>" alt="<?php echo $value['image'];?>">
                  <?php echo "<pre>" ;?>                  
                </div><br><br>
                <?php
                }?>
            </div>
            
        </div>
    </div>
   
</body>
</html>
<?php
    }else{
        header('location:login.php');
    }
    ?>