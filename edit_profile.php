<?php require_once("dashbord_nav.php");
session_start();

require_once("show_user_edit_data.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Profile</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <style>
        
        .con{
            
            margin-top:100px;
            width:70%;
        }
        .row{
            border: 1px ;
        }
        .col-sm-4, .col-sm-8{
            border: 1px solid blak;
            padding:20px;
        }

        .submit{
            width:30%;
        }
        .card{
            height:100%;
        }
        .inner-card{
            height:750px;
        }

        
        
    </style>
</head>
<body>
    <div class="container con">
        <div class="card inner-card">
            <div class="card-body">
            
            <div class="row">
            <div class="col-sm-4">
                    
                    <div class="card">
                        <div class="card-body">
                            <!-- <a href="">Change Password</a><br><br>
                            <a href="">Change Password</a><br><br>
                            <a href="">Change Password</a><br><br>
                            <a href="">Change Password</a><br><br> -->
                        </div>
                    </div>
                    
            </div>
            <div class="col-sm-8">
               <div class="card ">
               <div class="card-header">
               <h3> Edit Profile</h3>
               </div>
                   <div class="card-body">
                    <?php foreach ($result as $key => $value) {
                       //echo $value['Bio'];}exit;
                    ?>
                        <form action="edit_action.php" method="post">

                            <input type="hidden" name="id" value="<?php echo $value['id'];?>">
                        <b>Full Name:</b>
                            <div class="form-group">
                                <input type="text" name="full_name" value="<?php echo $value['full_name'];?>" class="form-control">
                            </div>
                        <b>Username:</b>
                            <div class="form-group">
                                <input type="text" name="username" value="<?php echo $value['username'];?>" class="form-control">
                            </div>
                        <b>Email</b>
                            <div class="form-group">
                                <input type="text" name="email" value="<?php echo $value['email'];?>" class="form-control">
                            </div>
                        <b>Contact:</b>
                            <div class="form-group">
                                <input type="text" name="phonenumber" value="<?php echo $value['mobile_number'];?>" class="form-control">
                            </div>
                        <b>Bio</b>
                            <div class="form-group">
                                <textarea name="bio" id="" cols="50" rows="4"><?php  echo $value['Bio'];?></textarea>
                            </div>
                        <b>Gender:</b>
                            <div class="form-group">
                                <input type="radio" name="gender" value="male" <?php echo ($value['gender']=='male')?'checked':'' ?>>Male
                                <input type="radio" name="gender" value="female"  <?php echo ($value['gender']=='female')?'checked':'' ?>>Female
                                <input type="radio" name="gender" value="other" <?php echo ($value['gender']=='other')?'checked':'' ?>>Other
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" value="Save" class="form-control btn-primary submit">
                            </div>
                        </form>
                    <?php } ?>
                   </div>
               </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>