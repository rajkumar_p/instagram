<!DOCTYPE html>
<html lang="en">
<head>
 
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    
        <style>
        .container-fluid {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            position: fixed;
            left: 0px;
            top: 0px;
            z-index:10;
            background:white;
        }
        ul li{
           padding:10px;
           font-size:18px;
        }

        
        
         /* .container{
            position:fixed;
            top:0;
        }  */
        
    </style>
</head>
<body>

<div class="container-fluid">
    <div class="container">
        <nav class="navbar">
            
            <div class="navbar-header">
                <h2><i class="fa fa-instagram"></i> | Instagram</h2>
            </div>

            <div class="search">
                <form class="form-inline">
                    <div class="md-form my-0">
                        <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                    </div>
                </form>
            </div>

                <ul class="nav ">
                    <li class="nav-item active"><a href="home_page.php">Home</a></li>
                    <li><a href="profile_page.php">Profile</a></li>
                    <li class="nav-item dropdown">
                        <a class=" dropdown-toggle"  data-toggle="dropdown" 
                        aria-expanded="false">Manage</a>
                        <div class="dropdown-menu dropdown-primary" >
                            <a class="dropdown-item" href="confirm_change_password.php">Change Password</a>
                            <a class="dropdown-item" href="change_profile_image.php">Change Profile </a>
                            <a class="dropdown-item" href="session_destroy.php">Logout</a>
                        </div>
                    </li> 
                </ul>
        </nav>
    </div>
</div>
  

</body>
</html>