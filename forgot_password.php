<?php require_once("nav_bar.php")?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forgot Password</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> -->
    <style>
    /* body{
            background-image:url('Grey-website-background.jpg');
            background-repeat: no-repeat;
            background-size:  100%;
            width: 100%;
            
            
       
    } */
        
        .card{
            width:40%;
            top:100px;
            margin:auto;
        }
        h1{
            font-family:serif ;
           
        }
        p{
            
            text-align:center;
            color:grey;
        }
        
        
        
        
        
    </style>
</head>

<body>
   
    

   <form action="username_check_forgot_action.php" method="post">
   
    <div class="container ">
        <div class="card  ">
            <div class="card-body">
                    <p>We can help you reset your password using your Instagram username, 
                    phone number, or the email address linked to your account.</p>

                    <hr class="hr-trxt" data-content="OR">
                    <div class="form-group">
                        <input type="text"  name="username" class="form-control" placeholder="Username" required>
                    </div>
                    
                    
                    <div class="form-group">
                        <input type="submit"  class="form-control btn-primary" value="Reset Password" id="">
                    </div>
            </div>
            </div>
    
        </div>
   </form>
   
        
   
</body>
</html>